import React, { Component } from 'react';

import { Button } from 'semantic-ui-react';

import EspecialidadeAPI from '../../api/EspecialidadeAPI';
import EspecialidadeModal from './EspecialidadeModal';
import EspecialidadesSearch from './EspecialidadesSearch';
import EspecialidadesTable from './EspecialidadesTable';

import $ from 'jquery';

class Especialidades extends Component {

    constructor() {
        super();

        this.state = {
          especialidades: [],
          rows: 0,
          especialidadesSearch: new EspecialidadesSearch()
        };
    }

    atualizarEspecialidades = () => {
      EspecialidadeAPI.searchAll(this.state.especialidadesSearch)
        .done((response) => {          
          this.setState({ 
            especialidades: response.especialidades,
            rows: response.rows
          });
        })
        .fail(() => { 
          console.log("Erro => getAll especialidades"); 
        });
    }

    limpar = () => {
      this.state.especialidadesSearch.reiniciarAtributos()
      this.setState({ especialidadesSearch: this.state.especialidadesSearch })
      this.atualizarEspecialidades()
    }

    buscar = () => {
      const especialidadesSearch = this.state.especialidadesSearch
      especialidadesSearch.currentPage = 0
      this.setState({ especialidadesSearch: especialidadesSearch })
      this.atualizarEspecialidades()
    }

    componentDidMount() {
      this.atualizarEspecialidades();

      $('#dataCadastroInicial').mask('00/00/0000', {
        placeholder: '__/__/____'
      });

      $('#dataCadastroFinal').mask('00/00/0000', {
        placeholder: '__/__/____'
      });
    }

    componentWillUnmount() {
    }

    handleInputChange = (e) => {
      const target = e.target;
      const value = target.value;
      const name = target.name;

      let especialidadesSearch = this.state.especialidadesSearch;
      especialidadesSearch.tableState = { 
        ...especialidadesSearch.tableState,
        [name]: value 
      };

      this.setState({ especialidadesSearch });
    }

    handleEspecialidadeUpdated(especialidade) {
      EspecialidadeAPI.put(especialidade)
        .done((response) => {
          this.atualizarEspecialidades();
        })
        .fail(() => {});
    }

    handleEspecialidadeDeleted(idEspecialidade) {
      EspecialidadeAPI.delete(idEspecialidade)
        .done((response) => {
          this.atualizarEspecialidades();
        })
        .fail(() => {});
    }

    render() {
      return (
        <div>
          <div className="search-form-header">Especialidades</div>

          <form className="search-form">
            <label className="search-form-label" style={{width: '180px'}}>Nome : </label>
            <input type="text" 
                   id="nomeEspecialidade" name="nomeEspecialidade" 
                   placeholder='Nome da Especialidade' 
                   className="search-form-input" style={{width: '300px'}}
                   value={this.state.especialidadesSearch.tableState.nomeEspecialidade}
                   onChange={this.handleInputChange} />
            <br />

            <label className="search-form-label" style={{width: '180px'}}>Data de Cadastro Início : </label>
            <input type="text" id="dataCadastroInicial" name="dataCadastroInicial"
                   className="search-form-input" style={{width: '300px'}} 
                   value={this.state.especialidadesSearch.tableState.dataCadastroInicial}
                   onChange={this.handleInputChange} />

            <label className="search-form-label" style={{width: '180px'}}>Data de Cadastro Fim : </label>
            <input type="text" id="dataCadastroFinal" name="dataCadastroFinal"
                   className="search-form-input" style={{width: '300px'}} 
                   value={this.state.especialidadesSearch.tableState.dataCadastroFinal}
                   onChange={this.handleInputChange} />
          </form>

          <div className="search-form-buttons">
            <Button color='blue' onClick={(e) => this.buscar()}>Buscar</Button>

            <Button color='blue' onClick={(e) => this.limpar()}>Limpar</Button>
            
            <EspecialidadeModal
                headerTitle='Adicionar Especialidade'
                buttonTriggerTitle='Adicionar'
                buttonSubmitTitle='Adicionar'
                buttonColor='green'
                atualizarEspecialidades={this.atualizarEspecialidades} />
          </div>
          
          <EspecialidadesTable 
            especialidades={this.state.especialidades}
            rows={this.state.rows}
            especialidadesSearch={this.state.especialidadesSearch}
            atualizarEspecialidades={this.atualizarEspecialidades}
            onEspecialidadeUpdated={this.handleEspecialidadeUpdated}
            onEspecialidadeDeleted={this.handleEspecialidadeDeleted} />
        </div>
      );
  }

}

export default Especialidades;