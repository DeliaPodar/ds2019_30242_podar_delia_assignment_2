import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { 
  Button,
  Modal,
  Form
} from 'semantic-ui-react'

import $ from 'jquery'
import _ from 'lodash'

import MedicoAPI from '../../api/MedicoAPI'

class MedicoModal extends Component {

  static propTypes = {
    icon: PropTypes.string,
    modalOpen: PropTypes.bool,
    hideFormCadastro: PropTypes.func,
    estados: PropTypes.array.isRequired,
    especialidades: PropTypes.array.isRequired
  }

  constructor(props) {
    super(props)

    this.state = this.getInitialState()
  }

  setInitialStateModal() {
    this.setState(this.getInitialState())
  }

  getInitialState() {
    return {
      nome_medico: '',
      cpf: '',
      telefone: '',
      data_nascimento: '',
      numero_crm: '',
      estado_crm_id: '',
      especialidade_id: ''
    }
  }

  onCpfFocus = () => {
    $("input[name='cpf']").mask('000.000.000-00', { reverse: false })
  }

  onTelefoneFocus = () => {
    $("input[name='telefone']").mask('(00) 00000-0000', { reverse: false })
  }

  onDataNascimentoFocus = () => {
    $("input[name='data_nascimento']").mask('00/00/0000', { reverse: false })
  }

  handleOpen = () => {
    if(this.props.medico) {
      let data_nascimento = this.props.medico.data_nascimento 
      if(data_nascimento.includes('-')) { 
        const partes = _.split(data_nascimento, '-')
        data_nascimento = `${partes[2]}/${partes[1]}/${partes[0]}`
      }

      let cpf = this.props.medico.cpf
      if(!cpf.includes('-')) {
        cpf = cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "$1.$2.$3-$4")
      }

      let telefone = this.props.medico.telefone
      if(!telefone.includes('-')) {
        telefone = telefone.replace(/(\d{2})(\d{5})(\d{4})/g, "($1) $2-$3")
      }

      this.setState({
        idMedico: this.props.medico.id,
        nome_medico: this.props.medico.nome_medico,
        cpf: cpf,
        telefone: telefone,
        data_nascimento: data_nascimento,
        numero_crm: this.props.medico.numero_crm,
        estado_crm_id: this.props.medico.estado_crm_id.toString(),
        especialidade_id: this.props.medico.especialidade_id.toString()
      })
    }
  }

  handleClose = () => {
    this.setInitialStateModal()
    this.props.hideFormCadastro()
  }

  handleSubmit = (e) => {
    e.preventDefault()

    let medico = { ...this.state }

    if(medico.idMedico) {
      medico = { ...medico, id: medico.idMedico }
      this.atualizar(medico)
    } else {
      this.salvar(medico)
    }
  }

  atualizar(medico) {
    MedicoAPI.put(medico)
      .done((response) => {
        this.props.hideFormCadastro();
      })
      .fail(() => {})
  }

  salvar(medico) {
    MedicoAPI.post(medico)
      .done((response) => {
        this.props.hideFormCadastro();
      })
      .fail(() => {})
  }

  handleInputChange = (e) => {
    const value = e.target.value;
    const name = e.target.name;

    this.setState({ [name]: value });
  }

  handleDropdownChange = (event, data) => {
    this.setState({ [data.name]: data.value });
  }

  render() {
    return (
      <Modal
        dimmer={true} size='small' closeIcon='close'
        open={this.props.modalOpen}
        onMount={() => this.handleOpen()}
        onClose={() => this.handleClose()}>
        <Modal.Header>{this.props.headerTitle}</Modal.Header>
        <Modal.Content>
          {this.state.idMedico && <span>ID do Médico : {this.state.idMedico}</span>}

          <Form onSubmit={this.handleSubmit}>
            <Form.Input
              type='text'
              name='nome_medico'
              label='Nome'
              placeholder='Nome'
              maxLength='255'
              required
              value={this.state.nome_medico}
              onChange={this.handleInputChange} />

            <Form.Dropdown
              name='especialidade_id'
              label='Especialidade'
              placeholder='Especialidade'
              required
              selection
              search
              value={this.state.especialidade_id}
              options={this.props.especialidades}
              onChange={this.handleDropdownChange} />
            
            <Form.Group>
              <Form.Input
                type='text'
                name='cpf'
                label='CPF'
                placeholder='CPF'
                value={this.state.cpf}
                onChange={this.handleInputChange}
                onFocus={this.onCpfFocus}
                width='8' />

              <Form.Input
                type='text'
                name='telefone'
                label='Telefone'
                placeholder='Telefone'
                value={this.state.telefone}
                onChange={this.handleInputChange}
                onFocus={this.onTelefoneFocus}
                width='8' />
            </Form.Group>

            <Form.Group>
              <Form.Input
                type='text'
                name='data_nascimento'
                label='Data de Nascimento'
                placeholder='Data de Nascimento'
                value={this.state.data_nascimento}
                onChange={this.handleInputChange}
                onFocus={this.onDataNascimentoFocus}
                width='4' />
            </Form.Group>

            <Form.Group>
              <Form.Input
                type='text'
                name='numero_crm'
                label='Número do CRM'
                placeholder='Número do CRM'
                value={this.state.numero_crm}
                onChange={this.handleInputChange} 
                width='4' />

              <Form.Dropdown 
                name="estado_crm_id"
                label="Estado do CRM"
                value={this.state.estado_crm_id}
                placeholder='Estado do CRM' 
                options={this.props.estados}
                onChange={this.handleDropdownChange}
                selection
                upward
                width='12' />
            </Form.Group>
            
            <div style={{float: 'left'}}>
              <Button className='ui-modal-button' color='red' onClick={this.handleClose}>Cancelar</Button>
              <Button className='ui-modal-button' color={this.props.buttonColor}>{this.props.buttonSubmitTitle}</Button>
            </div>
            <br />
          </Form>

        </Modal.Content>
      </Modal>
    )
  }

}

export default MedicoModal