import React, { Component } from 'react'

import { Button } from 'semantic-ui-react'

import $ from 'jquery'

import EspecialidadeAPI from '../../api/EspecialidadeAPI'
import EstadoAPI from '../../api/EstadoAPI'
import Pagination from '../common/Pagination'

import MedicoAPI from '../../api/MedicoAPI'
import MedicosSearch from './MedicosSearch'
import MedicosSearchForm from './MedicosSearchForm'
import MedicosTable from './MedicosTable'
import MedicoModal from './MedicoModal'

const limitOfRows = 10
const numberMaxPages = 9

class Medicos extends Component {

  constructor() {
    super()
    
    this.state = {
      rows: 0,
      medicos: [],
      medicosSearch: new MedicosSearch(),
      pagination: new Pagination(limitOfRows),
      
      searchNomeMedico: '',
      searchCpf: '',
      searchEstadoCrm: '',
      searchEspecialidade: '',

      showModalCadastro: false,
      showModalEdicao: false,

      especialidades: [],
      estados: [],
      estadosOptions: []
    }
  }

  componentDidMount() {
    this.atualizarTabela()
    this.fetchEspecialidades()
    this.fetchEstados()

    $('#searchCpf').mask('000.000.000-00', { reverse: false })
  }

  componentWillUnmount() { }

  atualizarTabela = () => {
    this.fetchMedicos()
  }

  fetchMedicos = () => {
    const searchParams = {
      nomeMedico: this.state.searchNomeMedico,
      cpf: this.state.searchCpf,
      estadoCrm: this.state.searchEstadoCrm,
      especialidade: this.state.searchEspecialidade
    }

    MedicoAPI.searchAll(this.state.medicosSearch, searchParams)
      .done((response) => {
        let medicos = JSON.parse(response.medicos)
        
        medicos = medicos.map((m) => {
          let cpf = m.cpf
          if(!cpf.includes('-')) {
            cpf = cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "$1.$2.$3-$4")
          }

          let telefone = m.telefone
          if(!telefone.includes('-')) {
            telefone = telefone.replace(/(\d{2})(\d{5})(\d{4})/g, "($1) $2-$3")
          }
          return {...m, cpf, telefone}
        })

        this.setState({
          medicos,
          rows: response.rows
        })
      })
      .fail(() => {})
  }

  fetchEspecialidades = () => {
    EspecialidadeAPI.getAll()
      .done((response) => {
        const options = response.map((e) => { 
          return { 'text': e.nome_especialidade, 'value': e.id.toString() } 
        })

        this.setState({
          especialidades: options
        })
      })
      .fail(() => {})
  }

  fetchEstados = () => {
    EstadoAPI.getAll()
      .done((response) => {

        const options = response.map((e) => {
          return { 'text': e.nome_estado, 'value': e.id.toString() }
        })

        this.setState({
          estados: response,
          estadosOptions: options
        })
      })
      .fail(() => {})
  }

  handleInputChange = (name, value) => {
    this.setState({ [name]: value })
  }

  buscar = () => {
    let search = this.state.medicosSearch.copy()
    search.currentPage = 0

    this.setState(
      { pagination: this.state.pagination.goToFirstPage(), medicosSearch: search },
      () => this.atualizarTabela()
    )
  }

  limpar = () => {
    this.setState({ 
      searchNomeMedico: '',
      searchCpf: '',
      searchEstadoCrm: '',
      searchEspecialidade: '',
      pagination: this.state.pagination.goToFirstPage(),
      medicosSearch: new MedicosSearch()
    }, () => this.atualizarTabela())
  }

  onPaginationChanged = (newPaginationState) => {
    // let search = this.state.medicosSearch.copy()
    // o comando abaixo equivale ao de cima...
    let search = Object.assign(new MedicosSearch(), this.state.medicosSearch)
    search.currentPage = newPaginationState.offset

    this.setState(
      { pagination: newPaginationState, medicosSearch: search }, 
      () => this.atualizarTabela()
    )
  }

  onOffsetChanged = (newOffset) => {
    let newPaginationState = new Pagination(limitOfRows)
    newPaginationState.offset = newOffset

    let search = Object.assign(new MedicosSearch(), this.state.medicosSearch)
    search.currentPage = newPaginationState.offset

    this.setState(
      { pagination: newPaginationState, medicosSearch: search }, 
      () => this.atualizarTabela()
    )
  }

  showFormCadastro = () => this.setState({ showModalCadastro: true })

  hideFormCadastro = () => {
    this.setState({ showModalCadastro: false }, () => this.atualizarTabela())
  }

  render() {
    return (
      <div>
        <div className="search-form-header">Listagem de Médicos</div>

        <MedicosSearchForm 
          medicosSearch={this.state.medicosSearch}
          
          searchNomeMedico={this.state.searchNomeMedico}
          searchCpf={this.state.searchCpf}
          searchEstadoCrm={this.state.searchEstadoCrm}
          searchEspecialidade={this.state.searchEspecialidade}

          handleInputChange={this.handleInputChange}
          especialidades={this.state.especialidades}
          estados={this.state.estados} />
        
        <div className="search-form-buttons">
          <Button color='blue' onClick={() => this.buscar()}>Buscar</Button>
          <Button color='blue' onClick={() => this.limpar()}>Limpar</Button>
          <Button color='green' onClick={() => this.showFormCadastro()}>Adicionar</Button>
        </div>

        <MedicosTable 
          medicos={this.state.medicos} 
          rows={this.state.rows}
          medicosSearch={this.state.medicosSearch}
          pagination={this.state.pagination}
          atualizarTabela={this.atualizarTabela} 
          paginationChanged={this.onPaginationChanged}
          offsetChanged={this.onOffsetChanged} 
          especialidades={this.state.especialidades}
          estados={this.state.estadosOptions} />

        <MedicoModal 
          modalOpen={this.state.showModalCadastro}
          headerTitle='Adicionar Médico'
          buttonSubmitTitle='Adicionar'
          buttonColor='green'
          hideFormCadastro={this.hideFormCadastro}
          especialidades={this.state.especialidades}
          estados={this.state.estadosOptions} />

      </div>
    )
  }

}

export default Medicos