
import Pagination from './Pagination';

const limit = 10

const rows_10 = 10
const rows_11 = 11
const rows_100 = 100
const rows_101 = 101


it('isPageBetweenFirstLastVisited() apartir de um offset no meio', () => {
  expect(
    new Pagination(limit).goToNextPage(rows_100).isPageBetweenFirstLastVisited(rows_100)
  ).toBeTruthy()
})

it('isPageBetweenFirstLastVisited() apartir do offset final', () => {
  expect(new Pagination(limit).goToLastPage(rows_100).isPageBetweenFirstLastVisited(rows_100)).toBeFalsy()
})

it('isPageBetweenFirstLastVisited() apartir do offset inicial', () => {
  expect(new Pagination(limit).isPageBetweenFirstLastVisited(rows_100)).toBeFalsy()
})

it('isPageLastVisited() apartir do ultimo offset', () => {
  expect(new Pagination(limit).goToLastPage(rows_100).isPageLastVisited(rows_100)).toBeTruthy()
})

it('isPageLastVisited() apartir do proximo offset', () => {
  expect(new Pagination(limit).goToNextPage(rows_100).isPageLastVisited(rows_100)).toBeFalsy()
})

it('isPageLastVisited() apartir do offset inicial', () => {
  expect(new Pagination(limit).isPageLastVisited(rows_10)).toBeTruthy()
})

it('isPageFirstVisited() apartir de um proximo offset', () => {
  expect(new Pagination(limit).goToNextPage(rows_100).isPageFirstVisited()).toBeFalsy()
})

it('isPageFirstVisited() apartir do offset inicial', () => {
  expect(new Pagination(limit).isPageFirstVisited()).toBeTruthy()
})



it('montar componente de paginacao apartir do proximo offset', () => {
  expect(new Pagination(limit).goToNextPage(rows_10).getCurrentState(rows_10))
    .toEqual([{ page: 1, offset: 0, currentPage: true }])

  expect(new Pagination(limit).goToNextPage(rows_11).getCurrentState(rows_11))
    .toEqual([{ page: 1, offset: 0, currentPage: false }, { page: 2, offset: 10, currentPage: true }])
})

it('montar componente de paginacao apartir do offset inicial', () => {
  expect(new Pagination(limit).getCurrentState(rows_10))
    .toEqual([{ page: 1, offset: 0, currentPage: true }])

  expect(new Pagination(limit).getCurrentState(rows_11))
    .toEqual([{ page: 1, offset: 0, currentPage: true }, { page: 2, offset: 10, currentPage: false }])
})



it('getCurrentPage() apartir do proximo offset', () => {
  const initial = new Pagination(limit)
  
  expect(initial.goToNextPage(rows_100).getCurrentPage()).toEqual(2)
})

it('getCurrentPage() apartir do offset inicial', () => {
  const initial = new Pagination(limit)
  expect(initial.getCurrentPage()).toEqual(1)
})

it('goToPreviousPage() apartir de um offset maior que o inicial', () => {
  const initial = new Pagination(limit)
  const previous = initial.goToNextPage(rows_100).goToPreviousPage()

  expect(previous.offset).toEqual(0)
  expect(previous.limit).toEqual(limit)

  expect(previous).toBeInstanceOf(Pagination)
  expect(previous).not.toBe(initial)
})

it('goToPreviousPage() apartir do offset inicial', () => {
  const initial = new Pagination(limit)
  const previous = initial.goToPreviousPage()

  expect(previous.offset).toEqual(0)
  expect(previous.limit).toEqual(limit)

  expect(previous).toBeInstanceOf(Pagination)
  expect(previous).toBe(initial)
})

it('goToLastPage() precisa receber o numero total de rows', () => {
  const p = new Pagination(limit)
  expect(() => p.goToLastPage()).toThrow()
})

it('goToLastPage() quando rows NAO for divisivel por zero', () => {
  const initial = new Pagination(limit)
  const last = initial.goToLastPage(rows_101)

  expect(last.offset).toEqual(100)
  expect(last.limit).toEqual(limit)

  expect(last).toBeInstanceOf(Pagination)
  expect(last).not.toBe(initial)
})

it('goToLastPage() quando rows for divisivel por zero', () => {
  const initial = new Pagination(limit)
  const last = initial.goToLastPage(rows_100)

  expect(last.offset).toEqual(90)
  expect(last.limit).toEqual(limit)

  expect(last).toBeInstanceOf(Pagination)
  expect(last).not.toBe(initial)
})

it('getTotalPages() para um total NAO divisivel por zero', () => {
  const p = new Pagination(limit)
  expect(p.getTotalPages(rows_101)).toEqual(11)
})

it('getTotalPages() para um total divisivel por zero', () => {
  const p = new Pagination(limit)
  expect(p.getTotalPages(rows_100)).toEqual(10)
})

it('goToNextPage() chamado varias vezes', () => {
  const p = new Pagination(limit)
  const next = p.goToNextPage(rows_100).goToNextPage(rows_100)

  expect(next.offset).toEqual(20)
  expect(next.limit).toEqual(limit)

  expect(next).toBeInstanceOf(Pagination)
  expect(next).not.toBe(p)
})

it('goToNextPage() apartir do offset inicial', () => {
  const p = new Pagination(limit)
  const next = p.goToNextPage(rows_11)

  expect(next.offset).toEqual(10)
  expect(next.limit).toEqual(limit)

  expect(next).toBeInstanceOf(Pagination)
  expect(next).not.toBe(p)
})

it('goToFirstPage() chamado varias vezes', () => {
  const p = new Pagination(limit)
  const first = p.goToFirstPage().goToFirstPage()

  expect(first.offset).toEqual(0)
  expect(first.limit).toEqual(limit)

  expect(first).toBeInstanceOf(Pagination)
  expect(first).not.toBe(p)
})

it('goToFirstPage() apartir do offset inicial', () => {
  const p = new Pagination(limit)
  const first = p.goToFirstPage()

  expect(first.offset).toEqual(0)
  expect(first.limit).toEqual(limit)

  expect(first).toBeInstanceOf(Pagination)
  expect(first).not.toBe(p)
})



it('offset inicial deve ser zero', () => {
  const p = new Pagination(limit)
  expect(p.offset).toEqual(0)
})

it('limit eh um parametro obrigatorio no constructor() senao for informado throw error', () => {
  expect(() => new Pagination()).toThrow()
})

it('limit eh um parametro obrigatorio no constructor()', () => {
  const p = new Pagination(limit)
  expect(p.limit).toEqual(limit)
})
