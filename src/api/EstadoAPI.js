import $ from 'jquery'

import API, { BASE_URL } from './API'

class EstadoAPI extends API {

  static getAll() {
    let request = $.ajax({ 
      url: `${BASE_URL}/api/estados`, 
      method: 'GET' 
    })

    return request
  }

}

export default EstadoAPI